const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();

const Attendance = require('./models/attendance');
const attendance = require('./models/attendance');

dbUrl = 'mongodb+srv://raja:raja2129@attendance.tbsr6.mongodb.net/attendance?retryWrites=true&w=majority'

mongoose.connect(dbUrl, {useNewUrlParser: true}, err => {
    if (err) {
        console.log('error connecting to database');
    } else {
        console.log('db connected');
    }
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
app.use(cors());

app.get('/', (req, res) => {
    console.log(req.query);
    new Attendance(req.query)
    .save().then(success => {
        console.log('successfull');
        res.send('<h1 style="color: green;">Successfull</h1>');
    }).catch(err => {
        console.log(err);
        res.send(err);
    });   
});

app.listen(2129, '0.0.0.0', 511, () => {
    console.log('app is listening to port 2129');
});