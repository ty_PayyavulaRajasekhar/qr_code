const mongoose = require('mongoose');
const schema = mongoose.Schema;

const attendanceSchema = new schema({
    fullName: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('attendance', attendanceSchema);