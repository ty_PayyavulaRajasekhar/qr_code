import { Component } from '@angular/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  date = new Date();
  // `https://attendance-a3590.firebaseio.com/attendance.json?date=${this.date.toISOString()}&name=Rajasekhar`;
  data = {
    text: '',
    data: {
      date: this.date.toISOString(),
      name: 'Rajasekhar'
    }
  };
  
  qrData = 'http://172.20.10.9:2129?fullName=raja&date=2020-09-29T09:22:55.772Z';
  constructor() {}

  
}
